-- phpMyAdmin SQL Dump
-- version 3.2.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 10, 2010 at 07:52 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `helptrack`
--

-- --------------------------------------------------------

--
-- Table structure for table `ht_needs`
--

CREATE TABLE IF NOT EXISTS `ht_needs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `owner` int(11) NOT NULL,
  `need_type_id` int(11) NOT NULL,
  `location` varchar(255) NOT NULL,
  `lat` varchar(255) NOT NULL,
  `lon` varchar(255) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ht_needs`
--


-- --------------------------------------------------------

--
-- Table structure for table `ht_need_types`
--

CREATE TABLE IF NOT EXISTS `ht_need_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ht_need_types`
--

INSERT INTO `ht_need_types` (`id`, `name`, `description`) VALUES
(1, 'Recursos Humanos', 'Personas que están disponibles como recursos Humanos.'),
(2, 'Vivienda de Emergencia', 'Viviendas que puedas suplir temporalmente problemas de alojamiento.');

-- --------------------------------------------------------

--
-- Table structure for table `ht_organizations`
--

CREATE TABLE IF NOT EXISTS `ht_organizations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `organization_role_id` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ht_organizations`
--

INSERT INTO `ht_organizations` (`id`, `name`, `phone`, `address`, `organization_role_id`) VALUES
(1, 'ChileAyuda.com', '+56(9)54321313', 'Huelen 164, Providencia, Santiago, Chile', 2),
(2, 'Digitales por Chile', '+56(9)54321313', 'Huelen 164, Providencia, Santiago, Chile', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ht_organization_roles`
--

CREATE TABLE IF NOT EXISTS `ht_organization_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ht_organization_roles`
--

INSERT INTO `ht_organization_roles` (`id`, `name`) VALUES
(1, 'Organización No Gubernamental'),
(2, 'Organización Ciudadana');

-- --------------------------------------------------------

--
-- Table structure for table `ht_projects`
--

CREATE TABLE IF NOT EXISTS `ht_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `location` varchar(100) NOT NULL,
  `state_id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `organization_id` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ht_projects`
--


-- --------------------------------------------------------

--
-- Table structure for table `ht_resources`
--

CREATE TABLE IF NOT EXISTS `ht_resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `resource_type_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `project_id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ht_resources`
--


-- --------------------------------------------------------

--
-- Table structure for table `ht_resource_types`
--

CREATE TABLE IF NOT EXISTS `ht_resource_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `ht_resource_types`
--

INSERT INTO `ht_resource_types` (`id`, `name`, `description`) VALUES
(1, 'Abrigo', 'Cantidad en kilogramos de frazadas, ropa para cama etc.'),
(2, 'Agua', 'Litros de agua.'),
(3, 'Alimentos', 'Cantidad en kilogramos de alimentos.'),
(4, 'Aseo general', 'Kilogramos de cloro, virutillas, detergentes, etc.'),
(5, 'Aseo personal', 'Kilos de jabón, shampoo, pasta de dientes, etc.'),
(6, 'Buses', 'Cantidad de transportes masivo de pasajeros.'),
(7, 'Camiones', 'Cantidad de vehiculos de carga mayor.'),
(8, 'Carpas', 'Cantidad de carpas.'),
(9, 'Colchonetas', 'Cantidad de colchonetas y aislantes para el piso.'),
(10, 'Combustibles', 'Kilos de carbón, bencina, parafina, etc. '),
(11, 'Constructores	 ', 'Cantidad de personas construyendo o reparando viviendas y/o estructuras.'),
(12, 'Enviados operativo de higiene ambiental y educación', 'Cantidad de voluntarios enviados para higiene ambiental y educación.'),
(13, 'Enviados operativo de medicina', 'Cantidad de voluntarios para operativos médicos.'),
(14, 'Enviados operativo psicosocial', 'Cantidad de voluntarios enviados para operativo psicosocial, esto incluye también, trabajo con niños.'),
(15, 'Escombristas ', 'Cantidad de personas limpiando y removiendo escombros.'),
(16, 'Evaluadores estructuras', 'Cantidad personas evaluando el estado de viviendas y estructuras.'),
(17, 'Otras maquinarias pesadas', 'Cantidad de otras maquinaria pesada, tales como martillos hidraulicos.'),
(18, 'Retroexcavadores ', 'Cantidad de maquinaria pesada, tales como: palas mecanicas, retroexcavadoras y bulldozer.'),
(19, 'Ropa', 'Kilos de ropa.'),
(20, 'Viviendas de emergencia ', 'Cantidad de  viviendas, tales como mediaguas, etc.'),
(21, 'Voluntarios en ayuda humanitaria', 'Voluntarios enviados para ayuda humanitaria (repartir alimentos, misionar entre otros).'),
(22, 'Iluminación', 'Cantidad de baterí­as, pilas, velas, etc.'),
(23, 'Herramientas', 'Cantidad de palas, chuzos, martillos y herramientas varias.');

-- --------------------------------------------------------

--
-- Table structure for table `ht_states`
--

CREATE TABLE IF NOT EXISTS `ht_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ht_states`
--


-- --------------------------------------------------------

--
-- Table structure for table `ht_users`
--

CREATE TABLE IF NOT EXISTS `ht_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `organization_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `ht_users`
--

INSERT INTO `ht_users` (`id`, `email`, `password`, `is_admin`, `first_name`, `last_name`, `organization_id`) VALUES
(2, 'matias@halles.cl', '81d3e964c656de00330a85b3324526634698c71b', 1, 'Matías', 'Halles', 2),
(3, 'test@chileayuda.com', '9e31049b7f42aae3327c8d5a04db65a78db745d4', 0, 'Monkey', 'Tester', 1),
(4, 'admin@chileayuda.com', 'b53d72d72814064b5533e540f94430118281c4ae', 1, 'Monkey', 'Admin', 2);
