<div class="needTypes view">
<h2><?php  __('NeedType');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $needType['NeedType']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $needType['NeedType']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $needType['NeedType']['description']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit NeedType', true), array('action' => 'edit', $needType['NeedType']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete NeedType', true), array('action' => 'delete', $needType['NeedType']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $needType['NeedType']['id'])); ?> </li>
		<li><?php echo $html->link(__('List NeedTypes', true), array('action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New NeedType', true), array('action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Needs', true), array('controller' => 'needs', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Need', true), array('controller' => 'needs', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Needs');?></h3>
	<?php if (!empty($needType['Need'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Description'); ?></th>
		<th><?php __('Owner'); ?></th>
		<th><?php __('Need Type Id'); ?></th>
		<th><?php __('Location'); ?></th>
		<th><?php __('Lat'); ?></th>
		<th><?php __('Lon'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($needType['Need'] as $need):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $need['id'];?></td>
			<td><?php echo $need['name'];?></td>
			<td><?php echo $need['description'];?></td>
			<td><?php echo $need['owner'];?></td>
			<td><?php echo $need['need_type_id'];?></td>
			<td><?php echo $need['location'];?></td>
			<td><?php echo $need['lat'];?></td>
			<td><?php echo $need['lon'];?></td>
			<td><?php echo $need['project_id'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller' => 'needs', 'action' => 'view', $need['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller' => 'needs', 'action' => 'edit', $need['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller' => 'needs', 'action' => 'delete', $need['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $need['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Need', true), array('controller' => 'needs', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
