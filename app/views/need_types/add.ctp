<div class="needTypes form">
<?php echo $form->create('NeedType');?>
	<fieldset>
 		<legend><?php __('Add NeedType');?></legend>
	<?php
		echo $form->input('name');
		echo $form->input('description');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List NeedTypes', true), array('action' => 'index'));?></li>
		<li><?php echo $html->link(__('List Needs', true), array('controller' => 'needs', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Need', true), array('controller' => 'needs', 'action' => 'add')); ?> </li>
	</ul>
</div>
