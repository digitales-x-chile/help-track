<div class="resources index">
<h2><?php __('Resources');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('name');?></th>
	<th><?php echo $paginator->sort('resource_type_id');?></th>
	<th><?php echo $paginator->sort('description');?></th>
	<th><?php echo $paginator->sort('project_id');?></th>
	<th><?php echo $paginator->sort('organization_id');?></th>
	<th><?php echo $paginator->sort('user_id');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($resources as $resource):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $resource['Resource']['id']; ?>
		</td>
		<td>
			<?php echo $resource['Resource']['name']; ?>
		</td>
		<td>
			<?php echo $html->link($resource['ResourceType']['name'], array('controller' => 'resource_types', 'action' => 'view', $resource['ResourceType']['id'])); ?>
		</td>
		<td>
			<?php echo $resource['Resource']['description']; ?>
		</td>
		<td>
			<?php echo $html->link($resource['Project']['name'], array('controller' => 'projects', 'action' => 'view', $resource['Project']['id'])); ?>
		</td>
		<td>
			<?php echo $html->link($resource['Organization']['name'], array('controller' => 'organizations', 'action' => 'view', $resource['Organization']['id'])); ?>
		</td>
		<td>
			<?php echo $html->link($resource['User']['id'], array('controller' => 'users', 'action' => 'view', $resource['User']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $resource['Resource']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $resource['Resource']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $resource['Resource']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $resource['Resource']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Resource', true), array('action' => 'add')); ?></li>
		<li><?php echo $html->link(__('List Resource Types', true), array('controller' => 'resource_types', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Resource Type', true), array('controller' => 'resource_types', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Organizations', true), array('controller' => 'organizations', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Organization', true), array('controller' => 'organizations', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
