<div class="resources form">
<?php echo $form->create('Resource');?>
	<fieldset>
 		<legend><?php __('Add Resource');?></legend>
	<?php
		echo $form->input('name');
		echo $form->input('resource_type_id');
		echo $form->input('description');
		echo $form->input('project_id');
		echo $form->input('organization_id');
		echo $form->input('user_id');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Resources', true), array('action' => 'index'));?></li>
		<li><?php echo $html->link(__('List Resource Types', true), array('controller' => 'resource_types', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Resource Type', true), array('controller' => 'resource_types', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Organizations', true), array('controller' => 'organizations', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Organization', true), array('controller' => 'organizations', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
