<div class="resourceTypes form">
<?php echo $form->create('ResourceType');?>
	<fieldset>
 		<legend><?php __('Edit ResourceType');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('name');
		echo $form->input('description');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('ResourceType.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('ResourceType.id'))); ?></li>
		<li><?php echo $html->link(__('List ResourceTypes', true), array('action' => 'index'));?></li>
		<li><?php echo $html->link(__('List Resources', true), array('controller' => 'resources', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Resource', true), array('controller' => 'resources', 'action' => 'add')); ?> </li>
	</ul>
</div>
