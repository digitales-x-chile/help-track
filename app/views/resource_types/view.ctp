<div class="resourceTypes view">
<h2><?php  __('ResourceType');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $resourceType['ResourceType']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $resourceType['ResourceType']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Description'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $resourceType['ResourceType']['description']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Edit ResourceType', true), array('action' => 'edit', $resourceType['ResourceType']['id'])); ?> </li>
		<li><?php echo $html->link(__('Delete ResourceType', true), array('action' => 'delete', $resourceType['ResourceType']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $resourceType['ResourceType']['id'])); ?> </li>
		<li><?php echo $html->link(__('List ResourceTypes', true), array('action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New ResourceType', true), array('action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Resources', true), array('controller' => 'resources', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Resource', true), array('controller' => 'resources', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Resources');?></h3>
	<?php if (!empty($resourceType['Resource'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Name'); ?></th>
		<th><?php __('Resource Type Id'); ?></th>
		<th><?php __('Description'); ?></th>
		<th><?php __('Project Id'); ?></th>
		<th><?php __('Organization Id'); ?></th>
		<th><?php __('User Id'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($resourceType['Resource'] as $resource):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $resource['id'];?></td>
			<td><?php echo $resource['name'];?></td>
			<td><?php echo $resource['resource_type_id'];?></td>
			<td><?php echo $resource['description'];?></td>
			<td><?php echo $resource['project_id'];?></td>
			<td><?php echo $resource['organization_id'];?></td>
			<td><?php echo $resource['user_id'];?></td>
			<td class="actions">
				<?php echo $html->link(__('View', true), array('controller' => 'resources', 'action' => 'view', $resource['id'])); ?>
				<?php echo $html->link(__('Edit', true), array('controller' => 'resources', 'action' => 'edit', $resource['id'])); ?>
				<?php echo $html->link(__('Delete', true), array('controller' => 'resources', 'action' => 'delete', $resource['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $resource['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $html->link(__('New Resource', true), array('controller' => 'resources', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
