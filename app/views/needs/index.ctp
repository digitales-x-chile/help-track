<div class="needs index">
<h2><?php __('Needs');?></h2>
<p>
<?php
echo $paginator->counter(array(
'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
));
?></p>
<table cellpadding="0" cellspacing="0">
<tr>
	<th><?php echo $paginator->sort('id');?></th>
	<th><?php echo $paginator->sort('name');?></th>
	<th><?php echo $paginator->sort('description');?></th>
	<th><?php echo $paginator->sort('owner');?></th>
	<th><?php echo $paginator->sort('need_type_id');?></th>
	<th><?php echo $paginator->sort('location');?></th>
	<th><?php echo $paginator->sort('lat');?></th>
	<th><?php echo $paginator->sort('lon');?></th>
	<th><?php echo $paginator->sort('project_id');?></th>
	<th class="actions"><?php __('Actions');?></th>
</tr>
<?php
$i = 0;
foreach ($needs as $need):
	$class = null;
	if ($i++ % 2 == 0) {
		$class = ' class="altrow"';
	}
?>
	<tr<?php echo $class;?>>
		<td>
			<?php echo $need['Need']['id']; ?>
		</td>
		<td>
			<?php echo $need['Need']['name']; ?>
		</td>
		<td>
			<?php echo $need['Need']['description']; ?>
		</td>
		<td>
			<?php echo $need['Need']['owner']; ?>
		</td>
		<td>
			<?php echo $html->link($need['NeedType']['name'], array('controller' => 'need_types', 'action' => 'view', $need['NeedType']['id'])); ?>
		</td>
		<td>
			<?php echo $need['Need']['location']; ?>
		</td>
		<td>
			<?php echo $need['Need']['lat']; ?>
		</td>
		<td>
			<?php echo $need['Need']['lon']; ?>
		</td>
		<td>
			<?php echo $html->link($need['Project']['name'], array('controller' => 'projects', 'action' => 'view', $need['Project']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $html->link(__('View', true), array('action' => 'view', $need['Need']['id'])); ?>
			<?php echo $html->link(__('Edit', true), array('action' => 'edit', $need['Need']['id'])); ?>
			<?php echo $html->link(__('Delete', true), array('action' => 'delete', $need['Need']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $need['Need']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
</div>
<div class="paging">
	<?php echo $paginator->prev('<< '.__('previous', true), array(), null, array('class'=>'disabled'));?>
 | 	<?php echo $paginator->numbers();?>
	<?php echo $paginator->next(__('next', true).' >>', array(), null, array('class' => 'disabled'));?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('New Need', true), array('action' => 'add')); ?></li>
		<li><?php echo $html->link(__('List Need Types', true), array('controller' => 'need_types', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Need Type', true), array('controller' => 'need_types', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
	</ul>
</div>
