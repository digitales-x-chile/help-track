<div class="needs form">
<?php echo $form->create('Need');?>
	<fieldset>
 		<legend><?php __('Add Need');?></legend>
	<?php
		echo $form->input('name');
		echo $form->input('description');
		echo $form->input('owner');
		echo $form->input('need_type_id');
		echo $form->input('location');
		echo $form->input('lat');
		echo $form->input('lon');
		echo $form->input('project_id');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Needs', true), array('action' => 'index'));?></li>
		<li><?php echo $html->link(__('List Need Types', true), array('controller' => 'need_types', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Need Type', true), array('controller' => 'need_types', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
	</ul>
</div>
