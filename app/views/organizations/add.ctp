<div class="organizations form">
<?php echo $form->create('Organization');?>
	<fieldset>
 		<legend><?php __('Add Organization');?></legend>
	<?php
		echo $form->input('name');
		echo $form->input('phone');
		echo $form->input('address');
		echo $form->input('organization_role_id');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('List Organizations', true), array('action' => 'index'));?></li>
		<li><?php echo $html->link(__('List Organization Roles', true), array('controller' => 'organization_roles', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Organization Role', true), array('controller' => 'organization_roles', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Projects', true), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Project', true), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
