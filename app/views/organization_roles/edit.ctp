<div class="organizationRoles form">
<?php echo $form->create('OrganizationRole');?>
	<fieldset>
 		<legend><?php __('Edit OrganizationRole');?></legend>
	<?php
		echo $form->input('id');
		echo $form->input('name');
	?>
	</fieldset>
<?php echo $form->end('Submit');?>
</div>
<div class="actions">
	<ul>
		<li><?php echo $html->link(__('Delete', true), array('action' => 'delete', $form->value('OrganizationRole.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $form->value('OrganizationRole.id'))); ?></li>
		<li><?php echo $html->link(__('List OrganizationRoles', true), array('action' => 'index'));?></li>
		<li><?php echo $html->link(__('List Organizations', true), array('controller' => 'organizations', 'action' => 'index')); ?> </li>
		<li><?php echo $html->link(__('New Organization', true), array('controller' => 'organizations', 'action' => 'add')); ?> </li>
	</ul>
</div>
