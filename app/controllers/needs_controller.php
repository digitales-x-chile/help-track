<?php
class NeedsController extends AppController {

	var $name = 'Needs';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->Need->recursive = 0;
		$this->set('needs', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Need', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('need', $this->Need->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Need->create();
			if ($this->Need->save($this->data)) {
				$this->Session->setFlash(__('The Need has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Need could not be saved. Please, try again.', true));
			}
		}
		$needTypes = $this->Need->NeedType->find('list');
		$projects = $this->Need->Project->find('list');
		$this->set(compact('needTypes', 'projects'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Need', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Need->save($this->data)) {
				$this->Session->setFlash(__('The Need has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Need could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Need->read(null, $id);
		}
		$needTypes = $this->Need->NeedType->find('list');
		$projects = $this->Need->Project->find('list');
		$this->set(compact('needTypes','projects'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Need', true));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Need->del($id)) {
			$this->Session->setFlash(__('Need deleted', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('The Need could not be deleted. Please, try again.', true));
		$this->redirect(array('action' => 'index'));
	}

}
?>