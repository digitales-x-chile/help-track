<?php
class ResourceTypesController extends AppController {

	var $name = 'ResourceTypes';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->ResourceType->recursive = 0;
		$this->set('resourceTypes', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid ResourceType', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('resourceType', $this->ResourceType->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ResourceType->create();
			if ($this->ResourceType->save($this->data)) {
				$this->Session->setFlash(__('The ResourceType has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ResourceType could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid ResourceType', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ResourceType->save($this->data)) {
				$this->Session->setFlash(__('The ResourceType has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ResourceType could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ResourceType->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for ResourceType', true));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->ResourceType->del($id)) {
			$this->Session->setFlash(__('ResourceType deleted', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('The ResourceType could not be deleted. Please, try again.', true));
		$this->redirect(array('action' => 'index'));
	}

}
?>