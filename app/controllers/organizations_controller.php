<?php
class OrganizationsController extends AppController {

	var $name = 'Organizations';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->Organization->recursive = 0;
		$this->set('organizations', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Organization', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('organization', $this->Organization->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Organization->create();
			if ($this->Organization->save($this->data)) {
				$this->Session->setFlash(__('The Organization has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Organization could not be saved. Please, try again.', true));
			}
		}
		$organizationRoles = $this->Organization->OrganizationRole->find('list');
		$this->set(compact('organizationRoles'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Organization', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Organization->save($this->data)) {
				$this->Session->setFlash(__('The Organization has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Organization could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Organization->read(null, $id);
		}
		$organizationRoles = $this->Organization->OrganizationRole->find('list');
		$this->set(compact('organizationRoles'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Organization', true));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Organization->del($id)) {
			$this->Session->setFlash(__('Organization deleted', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('The Organization could not be deleted. Please, try again.', true));
		$this->redirect(array('action' => 'index'));
	}

}
?>