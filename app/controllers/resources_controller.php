<?php
class ResourcesController extends AppController {

	var $name = 'Resources';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->Resource->recursive = 0;
		$this->set('resources', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid Resource', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('resource', $this->Resource->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->Resource->create();
			if ($this->Resource->save($this->data)) {
				$this->Session->setFlash(__('The Resource has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Resource could not be saved. Please, try again.', true));
			}
		}
		$resourceTypes = $this->Resource->ResourceType->find('list');
		$projects = $this->Resource->Project->find('list');
		$organizations = $this->Resource->Organization->find('list');
		$users = $this->Resource->User->find('list');
		$this->set(compact('resourceTypes', 'projects', 'organizations', 'users'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid Resource', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Resource->save($this->data)) {
				$this->Session->setFlash(__('The Resource has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The Resource could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Resource->read(null, $id);
		}
		$resourceTypes = $this->Resource->ResourceType->find('list');
		$projects = $this->Resource->Project->find('list');
		$organizations = $this->Resource->Organization->find('list');
		$users = $this->Resource->User->find('list');
		$this->set(compact('resourceTypes','projects','organizations','users'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for Resource', true));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->Resource->del($id)) {
			$this->Session->setFlash(__('Resource deleted', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('The Resource could not be deleted. Please, try again.', true));
		$this->redirect(array('action' => 'index'));
	}

}
?>