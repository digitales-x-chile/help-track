<?php
class NeedTypesController extends AppController {

	var $name = 'NeedTypes';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->NeedType->recursive = 0;
		$this->set('needTypes', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid NeedType', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('needType', $this->NeedType->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->NeedType->create();
			if ($this->NeedType->save($this->data)) {
				$this->Session->setFlash(__('The NeedType has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The NeedType could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid NeedType', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->NeedType->save($this->data)) {
				$this->Session->setFlash(__('The NeedType has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The NeedType could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->NeedType->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for NeedType', true));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->NeedType->del($id)) {
			$this->Session->setFlash(__('NeedType deleted', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('The NeedType could not be deleted. Please, try again.', true));
		$this->redirect(array('action' => 'index'));
	}

}
?>