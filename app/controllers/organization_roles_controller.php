<?php
class OrganizationRolesController extends AppController {

	var $name = 'OrganizationRoles';
	var $helpers = array('Html', 'Form');

	function index() {
		$this->OrganizationRole->recursive = 0;
		$this->set('organizationRoles', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid OrganizationRole', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('organizationRole', $this->OrganizationRole->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->OrganizationRole->create();
			if ($this->OrganizationRole->save($this->data)) {
				$this->Session->setFlash(__('The OrganizationRole has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The OrganizationRole could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid OrganizationRole', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->OrganizationRole->save($this->data)) {
				$this->Session->setFlash(__('The OrganizationRole has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The OrganizationRole could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->OrganizationRole->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for OrganizationRole', true));
			$this->redirect(array('action' => 'index'));
		}
		if ($this->OrganizationRole->del($id)) {
			$this->Session->setFlash(__('OrganizationRole deleted', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('The OrganizationRole could not be deleted. Please, try again.', true));
		$this->redirect(array('action' => 'index'));
	}

}
?>