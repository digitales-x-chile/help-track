<?php
class ResourceType extends AppModel {

	var $name = 'ResourceType';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array(
		'Resource' => array(
			'className' => 'Resource',
			'foreignKey' => 'resource_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>