<?php
class Need extends AppModel {

	var $name = 'Need';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $belongsTo = array(
		'NeedType' => array(
			'className' => 'NeedType',
			'foreignKey' => 'need_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Project' => array(
			'className' => 'Project',
			'foreignKey' => 'project_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

}
?>