<?php
class NeedType extends AppModel {

	var $name = 'NeedType';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array(
		'Need' => array(
			'className' => 'Need',
			'foreignKey' => 'need_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>