<?php
class OrganizationRole extends AppModel {

	var $name = 'OrganizationRole';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array(
		'Organization' => array(
			'className' => 'Organization',
			'foreignKey' => 'organization_role_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>